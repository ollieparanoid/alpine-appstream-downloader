# Alpine AppStream Downloader

This is a simple script for making Alpine Linux (and by consequence postmarketOS) comply with AppStream specification. The script has two tasks:

* Downloads AppStream [collection metadata](https://www.freedesktop.org/software/appstream/docs/chap-CollectionData.html) into the corresponding system location.
* Downloads and extracts [cached icons](https://www.freedesktop.org/software/appstream/docs/sect-AppStream-IconCache.html) into the corresponding system location.

## Installation

There is a simple Makefile at the root of the project that honors `DESTDIR` and `PREFIX` variables for installation of the script.
